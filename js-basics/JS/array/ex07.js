const movies = [
  { title: 'film1', year: 2018, rating: 4.6 },
  { title: 'film2', year: 2018, rating: 4.9 },
  { title: 'film3', year: 2018, rating: 3 },
  { title: 'film4', year: 2018, rating: 4.5 }
]
const titles = movies
  .filter(m => m.year === 2018 && m.rating >= 4)
  .sort((a, b) => a.rating - b.rating)
  .reverse()
  .map(m => m.title)
console.log(titles)
