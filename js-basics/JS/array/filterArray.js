const numbers = [1,-1,2,3]

const filtered = numbers.filter(function (value) {
  return value >=0 
  // return only if is positif
})

// with fat arrow

const filtered = numbers.filter(value => value >= 0)
console.log(filtered)